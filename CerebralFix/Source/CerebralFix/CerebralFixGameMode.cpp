// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CerebralFixGameMode.h"
#include "CerebralFixPlayerController.h"
#include "CerebralFixPawn.h"

ACerebralFixGameMode::ACerebralFixGameMode()
{
	// no pawn by default
	DefaultPawnClass = ACerebralFixPawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = ACerebralFixPlayerController::StaticClass();
}
